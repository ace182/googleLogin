import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';


import { GooglePlus } from '@ionic-native/google-plus';
import firebase from 'firebase';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  userProfile: any = null;

  constructor(public navCtrl: NavController, private googlePlus: GooglePlus) {
    firebase.auth().onAuthStateChanged( user => {
      if (user){
        this.userProfile = user;
      } else { 
        this.userProfile = null; 
      }
    });
 
  }
  loginUser(): void {
    this.googlePlus.login({
      'webClientId': '56184846404-gh36uqtrveebe1vhedepcmmbms4ka9q7.apps.googleusercontent.com',
      'offline': true
    }).then( res =>{ 
      firebase.auth().signInWithCredential(firebase.auth.GoogleAuthProvider.credential(res.idToken))
          .then( success => {
            console.log("Firebase success: " + JSON.stringify(success));
          })
          .catch( error => console.log("Firebase failure: " + JSON.stringify(error)));
        }).catch(err => console.error("Error: ", err));
    }
}